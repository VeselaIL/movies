//
//  Date+yearString.swift
//  MoviesApp
//
//  Created by Vesela Ilchevska on 14.07.21.
//

import Foundation

extension Date {
    var yearString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        return formatter.string(from: self)
    }
}
