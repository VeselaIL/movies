//
//  MoviesCollectionViewCell.swift
//  MoviesApp
//
//  Created by Vesela Ilchevska on 13.07.21.
//

import UIKit
import Kingfisher

final class MoviesCollectionViewCell: UICollectionViewCell {
    private enum Constants {
        static let imageSize: CGFloat = 180
        static let cornerRadius: CGFloat = 10
        static let standardSpacing: CGFloat = 8
        static let regularSpacing: CGFloat = 10
        static let tinySpacing: CGFloat = 4
        static let attachmentBounds = CGRect(x: 0, y: -2, width: 14, height: 14)
        static let secondaryFont: UIFont = .systemFont(ofSize: 13, weight: .thin)
        static let mainFont: UIFont = .boldSystemFont(ofSize: 13)
    }

    private var movieImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = Constants.mainFont
        return label
    }()
    
    private var ratingLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = Constants.secondaryFont
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private var releaseDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = Constants.secondaryFont
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var stackView: UIStackView = {
       let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = Constants.tinySpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    private func commonInit() {
        let separatorView = UIView()
        separatorView.backgroundColor = .white
        contentView.layer.cornerRadius = Constants.cornerRadius
        contentView.backgroundColor = .gray
        contentView.addSubview(movieImageView)
        contentView.addSubview(titleLabel)
        addSubview(stackView)
        stackView.addArrangedSubview(ratingLabel)
        stackView.addArrangedSubview(separatorView)
        stackView.addArrangedSubview(releaseDateLabel)
        
        NSLayoutConstraint.activate([
            movieImageView.topAnchor.constraint(equalTo: topAnchor,
                                                constant: Constants.regularSpacing),
            movieImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            movieImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            movieImageView.heightAnchor.constraint(equalToConstant: Constants.imageSize),
            
            titleLabel.topAnchor.constraint(equalTo: movieImageView.bottomAnchor,
                                            constant: Constants.standardSpacing),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            separatorView.widthAnchor.constraint(equalToConstant: 1),
            stackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            stackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
    }
    
    func configure(with data: Movie,
                   baseImageURL: String?) {
        titleLabel.text = data.title
        releaseDateLabel.text = data.releaseDate.yearString
        setupRatingLabelAttributedText(with: " \(data.voteAverage)")
        if let baseImageURL = baseImageURL {
            let url = URL(string: "\(baseImageURL)/\(data.posterPath)")
            movieImageView.kf.setImage(with: url)
        }
    }
    
    private func setupRatingLabelAttributedText(with text: String) {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: "star")
        attachment.bounds = Constants.attachmentBounds
        let attributedString = NSAttributedString(attachment: attachment)

        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attributedString)

        let text = NSMutableAttributedString(string: text)
        mutableAttributedString.append(text)
        
        ratingLabel.attributedText = mutableAttributedString
    }
}
