//
//  Movies.swift
//  MoviesApp
//
//  Created by Vesela Ilchevska on 13.07.21.
//

import Foundation

struct Movies: Decodable {
    let results: [Movie]
}

// MARK: - Result
struct Movie: Decodable {
    let posterPath: String
    let releaseDate: Date
    let title: String
    let voteAverage: Double

    enum CodingKeys: String, CodingKey {
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title
        case voteAverage = "vote_average"
    }
}
