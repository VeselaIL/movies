//
//  Images.swift
//  MoviesApp
//
//  Created by Vesela Ilchevska on 13.07.21.
//

import Foundation

struct Images: Decodable {
    let images: ImageConfiguration
}

struct ImageConfiguration: Decodable {
    let secureBaseURL: String
    let posterSizes: [String]
    
    enum CodingKeys: String, CodingKey {
        case secureBaseURL = "secure_base_url"
        case posterSizes = "poster_sizes"
    }
}
