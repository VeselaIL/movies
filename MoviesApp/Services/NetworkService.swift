//
//  NetworkService.swift
//  MoviesApp
//
//  Created by Vesela Ilchevska on 13.07.21.
//

import Alamofire

final class NetworkService {
    
    private enum Constants {
        static let apiKey = "a874a785f5b47354475d14574a969a7a"
        static let apiKeyQuery = "api_key"
        static let popularMoviesPath = "/movie/popular"
        static let configurationPath = "/configuration"
        static let host = "api.themoviedb.org"
        static let scheme = "https"
    }
    
    func getConfiguration(completion: @escaping (Result<Images, Error>) -> Void) {
        let getComponents = createURL(path: Constants.configurationPath)
        guard let validURL = getComponents.url else {
            return
        }
        AF.request(validURL)
            .responseData { response in
                if let data = response.data {
                    do {
                        let result = try JSONDecoder().decode(Images.self, from: data)
                        completion(.success(result))
                    } catch let error {
                        completion(.failure(error))
                    }
                }
            }
    }

    func getMovies(page: Int, completion: @escaping (Result<Movies, Error>) -> Void) {
        var getComponents = createURL(path: Constants.popularMoviesPath)
        getComponents.queryItems?.append(URLQueryItem(name: "page", value: "\(page)"))
        guard let validURL = getComponents.url else {
            return
        }
        let decoder = JSONDecoder()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        decoder.dateDecodingStrategy = .formatted(formatter)
        AF.request(validURL).responseData { response in
            if let data = response.data {
                do {
                    let result = try decoder.decode(Movies.self, from: data)
                    completion(.success(result))
                } catch let error {
                    completion(.failure(error))
                }
            }
        }
    }
    

    // MARK: Create URL Components
    private func createURL(path: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = Constants.scheme
        components.host = Constants.host
        components.path = "/3\(path)"
        components.queryItems = [URLQueryItem(name: Constants.apiKeyQuery, value: Constants.apiKey)]
        return components
    }
}
