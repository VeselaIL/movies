//
//  MainViewController.swift
//  MoviesApp
//
//  Created by Vesela Ilchevska on 13.07.21.
//

import UIKit

protocol MainViewControllerDegelate {
    func loadData(completion: @escaping () -> Void)
    func loadMovies(completion: @escaping () -> Void)

    var baseImageURL: String? { get }
    var movies: [Movie] { get }
}

final class MainViewController: UIViewController {
    private enum Constants {
        static let inset: CGFloat = 10
        static let cellsPerRow = 2
        static let minimumLineSpacing: CGFloat = 10
        static let minimumInteritemSpacing: CGFloat = 10
        static let padding: CGFloat = 30
        static let cellHeight: CGFloat = 250
    }
    
    fileprivate var viewModel: MainViewControllerDegelate!

    private var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: layout)
        collectionView.backgroundColor = .black
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(MoviesCollectionViewCell.self,
                                forCellWithReuseIdentifier: "\(MoviesCollectionViewCell.self)")
        return collectionView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        viewModel.loadData { [weak self] in
            self?.collectionView.reloadData()
        }
    }

    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MainViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let availableWidth = view.frame.width - Constants.padding
        let cellWidth = availableWidth / CGFloat(Constants.cellsPerRow)
        
        return CGSize(width: cellWidth, height: Constants.cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: Constants.inset,
                            left: Constants.inset,
                            bottom: Constants.inset,
                            right: Constants.inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return Constants.minimumInteritemSpacing
    }
}

// MARK: - UICollectionViewDelegate

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == collectionView.numberOfItems(inSection: indexPath.section) - 1 {
            viewModel.loadMovies { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
}

// MARK: - UICollectionViewDataSource

extension MainViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(MoviesCollectionViewCell.self)",
                                                            for: indexPath) as? MoviesCollectionViewCell,
              let movie = viewModel.movies[safe: indexPath.row] else {
            return UICollectionViewCell()
        }
        
        cell.configure(with: movie, baseImageURL: viewModel.baseImageURL)
        return cell
    }
}

struct MainViewControllerConfigurator {
    func createViewController() -> MainViewController? {
        let controller = MainViewController()
        let viewModel = MainViewModel()
        controller.viewModel = viewModel
        
        return controller
    }
}
