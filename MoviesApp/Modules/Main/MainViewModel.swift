//  
//  MainViewModel.swift
//  MoviesApp
//
//  Created by Vesela Ilchevska on 14.07.21.
//

import Foundation

final class MainViewModel {
    
    private let networkService: NetworkService
    var baseImageURL: String?
    var movies: [Movie] = []
    var pageCount: Int = 1

    init(networkService: NetworkService = NetworkService()) {
        self.networkService = networkService
    }
}

// MARK: - MainViewControllerDegelate

extension MainViewModel: MainViewControllerDegelate {
    
    func loadMovies(completion: @escaping () -> Void) {
        networkService.getMovies(page: pageCount) { result in
            switch result {
            case .success(let value):
                self.movies.append(contentsOf: value.results)
                self.pageCount += 1
                completion()
            case .failure(let error):
                print(error)
            }
        }
    }

    func loadData(completion: @escaping () -> Void) {
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        networkService.getConfiguration { result in
            switch result {
            case .success(let value):
                self.baseImageURL = "\(value.images.secureBaseURL)\(value.images.posterSizes[2])"
            case .failure(let error):
                print(error)
            }
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        networkService.getMovies(page: pageCount) { result in
            switch result {
            case .success(let value):
                self.movies.append(contentsOf: value.results)
                self.pageCount += 1
            case .failure(let error):
                print(error)
            }
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main, execute: {
           completion()
        })
    }
}
